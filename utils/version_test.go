package utils

import (
	"fmt"
	"strings"
	"testing"
)

func TestVersionStringToNumber(t *testing.T) {
	var value uint32
	value = VersionStringToNumber("1.0.5")
	if value != 10005 {
		fmt.Println("10005 but ", value)
		t.Fail()
	}

	value = VersionStringToNumber("1.0.1")
	if value != 10001 {
		fmt.Println(value)
		t.Fail()
	}

	value = VersionStringToNumber("1.0.122")
	if value != 0 {
		fmt.Println(value)
		t.Fail()
	}

	value = VersionStringToNumber("99.99.98")
	if value != 999998 {
		fmt.Println(value)
		t.Fail()
	}

	value = VersionStringToNumber("0.0.1")
	if value != 1 {
		fmt.Println(value)
		t.Fail()
	}

	value = VersionStringToNumber("q.0.1")
	if value != 0 {
		fmt.Println(value)
		t.Fail()
	}

	// t.Fail()
}

func TestVersionNumberToString(t *testing.T) {
	var version string = ""

	version = VersionNumberToString(1)
	fmt.Println(version)
	if strings.Compare(version, "0.0.1") != 0 {
		t.Fail()
	}

	version = VersionNumberToString(999999)
	fmt.Println(version)
	if strings.Compare(version, "99.99.99") != 0 {
		t.Fail()
	}

	version = VersionNumberToString(1000000)
	fmt.Println(version)
	if strings.Compare(version, "") != 0 {
		t.Fail()
	}

	version = VersionNumberToString(100000)
	fmt.Println(version)
	if strings.Compare(version, "10.0.0") != 0 {
		t.Fail()
	}
	// t.Fail()
}
