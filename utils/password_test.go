package utils

import "testing"

func TestGetPasswordLever(t *testing.T) {
	var level int32

	level = GetPasswordLever("1234")
	if level != 0 {
		t.Error("\"1234\" level error, is", level)
		t.Fail()
	}

	level = GetPasswordLever("123456789")
	if level != 1 {
		t.Error("\"123456789\" level error, is", level)
		t.Fail()
	}

	level = GetPasswordLever("123456789a")
	if level != 2 {
		t.Error("\"123456789a\" level error, is", level)
		t.Fail()
	}

	level = GetPasswordLever("123456789aB")
	if level != 3 {
		t.Error("\"123456789aB\" level error, is", level)
		t.Fail()
	}

	level = GetPasswordLever("12345!6789")
	if level != 2 {
		t.Error("\"12345!6789\" level error, is", level)
		t.Fail()
	}

	level = GetPasswordLever("12345!Ab67")
	if level != 4 {
		t.Error("\"12345!Ab67\" level error, is", level)
		t.Fail()
	}
}

func TestPassword(t *testing.T) {
	loginPwd := "e32bc17f618a59932a51f4161202e2b7"
	uid := uint32(1673491265)

	encodePwd := GeneratePassword(uid, loginPwd)
	if len(encodePwd) == 0 {
		t.Error("encodePwd:" + encodePwd)
		t.Fail()
	}

	if !ComparePassword(uid, loginPwd, encodePwd) {
		t.Error("encodePwd:" + encodePwd)
		t.Fail()
	}

	if ComparePassword(uint32(1673491264), loginPwd, encodePwd) {
		t.Error("encodePwd:" + encodePwd)
		t.Fail()
	}

	if ComparePassword(uid, "C4760D05B23920C4297DB123D52BC33F", encodePwd) {
		t.Error("encodePwd:" + encodePwd)
		t.Fail()
	}
}
