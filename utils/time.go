package utils

import (
	"strconv"
	"time"
)

func GetNowTimestampString() string {
	now := time.Now().Unix()
	str := strconv.FormatUint(uint64(now), 10)
	return str
}
