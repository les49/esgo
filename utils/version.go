package utils

import (
	"fmt"
	"strconv"
	"strings"
)

// version xx.xx.xx
const (
	VERSION_FIELD_COUNT     = 3
	VERSION_FIELD_VALUE_LEN = 2
	VERSION_NUMBER_MAX      = 999999
)

func VersionStringToNumber(version string) uint32 {
	var value uint32 = 0
	s := strings.Split(version, ".")
	if len(s) != VERSION_FIELD_COUNT {
		return value
	}

	for i := 0; i < len(s); i++ {
		if len(s[i]) > VERSION_FIELD_VALUE_LEN {
			return 0
		}
		v, err := strconv.Atoi(s[i])
		if err != nil {
			return 0
		}

		value = (value * 100) + uint32(v)
	}
	return value
}

func VersionNumberToString(version uint32) string {
	var versionString string = ""
	if version > VERSION_NUMBER_MAX {
		return versionString
	}

	var v uint32 = version
	for i := 0; i < VERSION_FIELD_COUNT; i++ {
		versionString = fmt.Sprintf(".%d", v%100) + versionString
		v = v / 100
	}

	return versionString[1:]
}
