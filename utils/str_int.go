package utils

func HexByte2Char(b byte) uint8 {
	if b < 10 {
		return uint8('0' + b)
	}

	if b < 16 {
		return uint8('a' + (b - 10))
	}

	return 'x'
}

func Int32ToHexString(v int32) string {
	charArray := make([]byte, 16)
	for i := 0; i < 8; i++ {
		charArray[i] = HexByte2Char(byte((v >> ((7 - i) * 4) & 0x0F)))
	}

	return string(charArray)
}

func Int64ToHexString(v int64) string {
	charArray := make([]byte, 32)
	for i := 0; i < 16; i++ {
		charArray[i] = HexByte2Char(byte((v >> ((15 - i) * 4) & 0x0F)))
	}

	return string(charArray)
}
