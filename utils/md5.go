package utils

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
)

func MD5V(str []byte) string {
	h := md5.New()
	h.Write(str)
	return hex.EncodeToString(h.Sum(nil))
}

func CalcFileMd5(f io.Reader) string {
	md5hash := md5.New()
	if _, err := io.Copy(md5hash, f); err != nil {
		return ""
	}

	md5hash.Sum(nil)
	md5str := fmt.Sprintf("%x", md5hash.Sum(nil))

	return md5str
}
