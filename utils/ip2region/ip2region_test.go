package ip2region

import "testing"

func TestGetRegion(t *testing.T) {
	var region string
	ip2reg := NewWithContentBuffer("ip2region.xdb")

	// 江苏省南京市
	region = ip2reg.GetRegionString("8.8.8.8")
	if len(region) == 0 {
		t.FailNow()
	}
	t.Log(region)

	// 美国加利福尼亚州圣何塞
	region = ip2reg.GetRegionString("185.218.4.139")
	if len(region) == 0 {
		t.FailNow()
	}
	t.Log(region)

	// 云南 昆明
	region = ip2reg.GetRegionString("222.172.134.138")
	if len(region) == 0 {
		t.FailNow()
	}
	t.Log(region)

	// 内网地址
	region = ip2reg.GetRegionString("172.25.90.197")
	if len(region) == 0 {
		t.FailNow()
	}
	t.Log(region)
}
