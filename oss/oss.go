package oss

type Oss interface {
	PutObject(bucketName, objectName string, fileContent []byte) error
	GetObject(bucketName, objectName string) ([]byte, error)
	DelObject(bucketName, objectName string) error
	ExistBucket(bucketName string) bool
	ExistObject(bucketName, objectName string) bool
}
