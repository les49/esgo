package date

import "time"

func CalcWeek(t time.Time) uint32 {
	yearDay := t.YearDay()
	yesterdayYearEndDay := t.AddDate(0, 0, -yearDay+1)
	dayInWeek := int(yesterdayYearEndDay.Weekday())
	firstWeekDays := 7
	if dayInWeek != 0 {
		firstWeekDays = 7 - dayInWeek
	}

	week := 0
	if yearDay <= firstWeekDays {
		week = 1
	} else {
		plusDay := 0
		if (yearDay-firstWeekDays)%7 > 0 {
			plusDay = 1
		}
		week = (yearDay-firstWeekDays)/7 + 1 + plusDay
	}

	return uint32(week)
}

// return 20230301
func GetDailyId() uint32 {
	t := time.Now()
	return uint32(t.Year()*10000) + uint32(t.Month()*100) + uint32(t.Day())
}

func GetWeeklyId() uint32 {
	t := time.Now()
	return uint32(t.Year()*100) + CalcWeek(t)
}
