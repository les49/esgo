package date

import (
	"fmt"
	"testing"
	"time"
)

func TestCalcWeek(t *testing.T) {
	timeStr := "2023-03-01 00:00:00"
	tm, err := time.ParseInLocation("2006-01-02 15:04:05", timeStr, time.Local)
	if err != nil {
		t.Fail()
	}
	week := CalcWeek(tm)
	if week != 9 {
		fmt.Println(timeStr, "week", week)
		t.Fail()
	}

	timeStr = "2023-02-22 00:00:00"
	tm, err = time.ParseInLocation("2006-01-02 15:04:05", timeStr, time.Local)
	if err != nil {
		t.Fail()
	}
	week = CalcWeek(tm)
	if week != 8 {
		fmt.Println(timeStr, "week", week)
		t.Fail()
	}

	timeStr = "2023-01-01 00:00:00"
	tm, err = time.ParseInLocation("2006-01-02 15:04:05", timeStr, time.Local)
	if err != nil {
		t.Fail()
	}
	week = CalcWeek(tm)
	if week != 1 {
		fmt.Println(timeStr, "week", week)
		t.Fail()
	}

	timeStr = "2022-12-31 00:00:00"
	tm, err = time.ParseInLocation("2006-01-02 15:04:05", timeStr, time.Local)
	if err != nil {
		t.Fail()
	}
	week = CalcWeek(tm)
	if week != 53 {
		fmt.Println(timeStr, "week", week)
		t.Fail()
	}

}

func TestGetDailyId(t *testing.T) {
	tm := time.Now()
	id := uint32(tm.Year()*10000) + uint32(tm.Month()*100) + uint32(tm.Day())

	dailyId := GetDailyId()
	if dailyId != id {
		fmt.Println(dailyId, id)
		t.FailNow()
	}

	idStr := fmt.Sprintf("%d", dailyId)
	if len(idStr) != 8 {
		fmt.Println("idStr:", idStr, "length not is 8")
		t.FailNow()
	}
}

func TestGetWeeklyId(t *testing.T) {
	weekId := GetWeeklyId()
	idStr := fmt.Sprintf("%d", weekId)
	if len(idStr) == 6 {
		fmt.Println("idStr:", idStr, "length not is 6")
		t.FailNow()
	}
}
