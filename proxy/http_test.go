package proxy

import (
	"testing"
	"time"

	"github.com/zeromicro/go-zero/core/logx"
)

type testPostData struct {
	Adata int `json:"a"`
}

func TestHttpSend(t *testing.T) {
	data := testPostData{
		Adata: 1,
	}

	logx.Disable()

	hp := NewHttpProxy()
	for i := 0; i < 1000; i++ {
		data.Adata++
		err := hp.SendData("1234", "http://httpxbin.org/post", data)
		if err != nil {
			t.Log(err)
			t.FailNow()
		}
	}

	time.Sleep(time.Second * 3)
	for i := 0; i < 100; i++ {
		data.Adata++
		err := hp.SendData("1234", "http://httpxbin.org/post", data)
		if err == nil {
			t.FailNow()
		}
	}
}
