package systeminfo

import "testing"

func TestReadInfo(t *testing.T) {
	sysData := ReadServerData()
	if sysData == nil {
		t.FailNow()
	}
	t.Log(sysData)

	sysInfo := ReadServerInfo()
	if sysInfo == nil {
		t.FailNow()
	}
	t.Log(sysInfo)
}
