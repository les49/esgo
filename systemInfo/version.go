package systeminfo

import "fmt"

type VersionInfo struct {
	Version   string `json:"version"`
	BuildDate string `json:"buildDate"`
}

func getBuildDate() string {
	return fmt.Sprintf("%s %s", "2023-04-22", "16:02:01")
}

func ReadVersionInfo() *VersionInfo {
	return &VersionInfo{
		Version:   "1.0.0",
		BuildDate: getBuildDate(),
	}
}
