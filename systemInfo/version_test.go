package systeminfo

import "testing"

func TestReadVersionInfo(t *testing.T) {
	versionInfo := ReadVersionInfo()
	if versionInfo == nil {
		t.FailNow()
	}
	t.Log(versionInfo)
}
