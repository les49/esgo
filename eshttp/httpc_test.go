package eshttp

import (
	"encoding/json"
	"testing"
)

type TestHttpPostBodyReq struct {
	PostData string `json:"data"`
}

type TestHttpPostBodyResp struct {
	Url string `json:"url"`
}

type TestEmqxClientBodyResp struct {
	Code    string `json:"code,optional"`
	IpAddr  string `json:"ip_address,optional"`
	Port    uint32 `json:"port,optional"`
	SentOct uint64 `json:"send_oct,optional"`
	RecvOct uint64 `json:"recv_oct,optional"`
}

// {"mac":"061060332765","type":"set", "cmd":"openDoor","data":{"openDelay":3}}
type TestOpenDoorData struct {
	OpenDelay uint32 `json:"openDelay"`
}
type TestOpenDoorReq struct {
	Mac  string           `json:"mac"`
	Type string           `json:"type"`
	Cmd  string           `json:"cmd"`
	Data TestOpenDoorData `json:"data"`
}

func TestPostJsonRequest(t *testing.T) {
	url := "http://httpbin.org/post"
	body := &TestHttpPostBodyReq{}
	var bodyResp TestHttpPostBodyResp
	resp, err := PostJsonRequest(url, body)
	if err != nil || resp == nil {
		t.FailNow()
	}
	t.Log("resp:", string(resp))

	err = json.Unmarshal(resp, &bodyResp)
	if err != nil {
		t.FailNow()
	}
	t.Log("bodyResp:", bodyResp)
}

func TestGetWithBasicAuth(t *testing.T) {
	url := "http://192.168.22.174:18083/api/v5/clients/mqttx_16c2d897"
	username := "603ac5eea8b7e0d3"
	password := "9CcaXMwwmckLpkGpAuHGK1LhDH9A1Cz9BpXy9A5LEdkIqxK"
	var bodyResp TestEmqxClientBodyResp

	resp, err := GetWithBasicAuth(url, username, password)
	if err != nil || resp == nil {
		t.FailNow()
	}
	t.Log("resp:", string(resp))

	err = json.Unmarshal(resp, &bodyResp)
	if err != nil {
		t.FailNow()
	}
	t.Log("bodyResp:", bodyResp)
}

func TestPostJsonRequestWithHeaders(t *testing.T) {
	url := "http://iot.aihardware.cn/api/v1/cmd"
	body := &TestOpenDoorReq{
		Mac:  "061060332765",
		Type: "set",
		Cmd:  "openDoor",
		Data: TestOpenDoorData{
			OpenDelay: 3,
		},
	}

	headers := make([]*HttpHeader, 0, 2)
	headers = append(headers, &HttpHeader{
		Key:   "Content-Type",
		Value: "application/json",
	})
	headers = append(headers, &HttpHeader{
		Key:   "token",
		Value: "4ee67a0c3b1a77728353ff17804aed63",
	})

	respBody, err := PostJsonRequestWithHeaders(url, headers, body)
	if err != nil || respBody == nil {
		t.FailNow()
	}
	t.Log(string(respBody))

}
