package eshttp

import (
	"errors"
	"io/ioutil"
	"net/http"
	"strings"

	"gitee.com/les49/esgo/eshttp/internal/header"
)

func withJsonBody(r *http.Request) bool {
	return r.ContentLength > 0 && strings.Contains(r.Header.Get(header.ContentType), header.ApplicationJson)
}

func ReadJsonBody(r *http.Request) ([]byte, error) {
	if !withJsonBody(r) {
		return nil, errors.New("content type error")
	}

	jsonBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	return jsonBody, nil
}
