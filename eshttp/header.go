package eshttp

import "context"

type UserCtx struct {
	UserId uint32 // 用户id
	IP     string // 用户IP地址
	RoleId uint32 // 角色id
	OS     string // 操作系统信息
}

func SetUserCtx(ctx context.Context, userId uint32, osInfo, ip string, roleId uint32) context.Context {
	return context.WithValue(ctx, UserUid, &UserCtx{UserId: userId, OS: osInfo, IP: ip, RoleId: roleId})
}

func GetUserCtx(ctx context.Context) *UserCtx {
	userCtx, ok := ctx.Value(UserUid).(*UserCtx)
	if !ok {
		panic("GetUserCtx get UserCtx failed")
	}
	return userCtx
}

func IsRole(ctx context.Context, roleId uint32) bool {
	userCtx, ok := ctx.Value(UserUid).(*UserCtx)
	if !ok {
		panic("GetUserCtx get UserCtx failed")
	}

	if userCtx.RoleId == roleId {
		return true
	}

	return false
}
