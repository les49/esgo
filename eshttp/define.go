package eshttp

const (
	UserToken    string = "esgo-token"
	UserIp       string = "esgo-ip"
	UserUid      string = "esgo-uid"
	UserSetToken string = "esgo-set-token"
)
