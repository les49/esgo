package errors

var (
	Upload             = NewCodeError(FILE_ERROR+1, "上传失败")
	FileMd5Check       = NewCodeError(FILE_ERROR+2, "MD5比较失败")
	FileExist          = NewCodeError(FILE_ERROR+3, "文件已经存在")
	FileCreate         = NewCodeError(FILE_ERROR+4, "文件创建失败")
	FileNotExist       = NewCodeError(FILE_ERROR+5, "文件不存在")
	FileReadException  = NewCodeError(FILE_ERROR+6, "文件读取异常")
	FileWriteException = NewCodeError(FILE_ERROR+7, "文件写取异常")
	FileFormat         = NewCodeError(FILE_ERROR+8, "文件格式错误")
)
