package errors

var (
	DeviceSuccess     = NewCodeError(0, "success")
	DeviceNotExist    = NewCodeError(PROTOCOL_ERROR, "not exist")
	DeviceAuthExpire  = NewCodeError(PROTOCOL_ERROR+1, "auth expire")
	DeviceMacNotMatch = NewCodeError(PROTOCOL_ERROR+2, "mac not match")
	DeviceDisable     = NewCodeError(PROTOCOL_ERROR+3, "disable")
)
