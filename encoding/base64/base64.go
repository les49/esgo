package base64

import (
	"encoding/base64"
)

func Base64StdEncode(srcByte []byte) string {
	encoding := base64.StdEncoding.EncodeToString(srcByte)
	return encoding
}

func Base64StdDecode(srcString string) []byte {
	sDec, err := base64.StdEncoding.DecodeString(srcString)
	if err != nil {
		return nil
	}
	return sDec
}
