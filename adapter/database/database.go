package database

import "github.com/jinzhu/gorm"

type DatabaseInterface interface {
	Setup(dialect string) error
	GetDb() *gorm.DB
	Close() error
}

var Database DatabaseInterface

func init() {
	Database = &DbMysql{}
}
