package database

import (
	"database/sql"

	_ "github.com/taosdata/driver-go/v3/taosRestful"
)

type TDengine struct {
	Dsn string
	*sql.DB
}

func NewTDengine(DataSource string) (*TDengine, error) {
	taos, err := sql.Open("taosRestful", DataSource)
	if err != nil {
		return nil, err
	}
	return &TDengine{
		Dsn: DataSource,
		DB:  taos,
	}, nil
}
