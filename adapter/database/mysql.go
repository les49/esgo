package database

import (
	"errors"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var mysql *gorm.DB

type DbMysql struct{}

func (*DbMysql) Setup(dialect string) error {
	if mysql != nil {
		return errors.New("open again")
	}

	db, err := gorm.Open("mysql", dialect)
	if err != nil {
		return err
	}

	mysql = db
	return nil
}

func (*DbMysql) GetDb() *gorm.DB {
	return mysql
}

func (*DbMysql) Close() error {
	if mysql == nil {
		return errors.New("already close")
	}

	mysql.Close()
	mysql = nil
	return nil
}
