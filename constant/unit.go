package constant

const (
	BYTE_UNIT_KB = (1024)
	BYTE_UNIT_MB = (BYTE_UNIT_KB * 1024)
	BYTE_UNIT_GB = (BYTE_UNIT_MB * 1024)
)
