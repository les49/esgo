package constant

const (
	TopicPublishOneDevice = "ser2dev/v1/%s/%s/sub"
	TopicPublishProduct   = "ser2dev/v1/%s/#/sub"

	TopicSubscribeDevice = "dev2ser/v1/+/+/pub"
)