package constant

const (
	USER_ROLE_ID_ADMIN   = 0 // 管理员
	USER_ROLE_ID_TEACHER = 1 // 教师
	USER_ROLE_ID_STUDENT = 2 // 学生
)
