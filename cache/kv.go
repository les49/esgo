package cache

import (
	kvCache "github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/kv"
)

type Kv struct {
	store kv.Store
}

func NewKv(c kvCache.ClusterConf) *Kv {
	kv := &Kv{
		store: kv.NewStore(c),
	}

	return kv
}

func (k *Kv) Set(key, value string) error {
	return k.store.Set(key, value)
}

func (k *Kv) Get(key, value string) (string, error) {
	return k.store.Get(key)
}

func (k *Kv) Expire(key string, seconds uint32) error {
	return k.store.Expire(key, int(seconds))
}
