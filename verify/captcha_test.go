package verify

import "testing"

func TestNewCaptcha(t *testing.T) {

	var result *CaptchaResult
	var err error

	// test digit
	conf := &CaptchaConf{
		Type:    "digit",
		Width:   100,
		Height:  40,
		KeyLong: 6}

	result, err = NewCaptcha(conf)
	if err != nil || result == nil {
		t.Error(result)
		t.Error(err)
		t.FailNow()
	}

	// test math
	conf.Type = "math"
	result, err = NewCaptcha(conf)
	if err != nil || result == nil {
		t.Error(result)
		t.Error(err)
		t.FailNow()
	}

	// test not support
	conf.Type = "not"
	result, err = NewCaptcha(conf)
	if err == nil || result != nil {
		t.Error(result)
		t.Error(err)
		t.FailNow()
	}
}
