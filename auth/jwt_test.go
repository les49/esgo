package auth

import "testing"

func TestJwtToken(t *testing.T) {
	secretKey := "bd879a37-c7b4-40c3-92d6-6bfc55d5457a"
	expSeconds := int64(3600)

	userData := map[string]interface{}{
		"uid": 1234,
	}

	jwtToken, err := NewJwtToken(secretKey, expSeconds, userData)
	if err != nil {
		t.FailNow()
	}
	t.Log(jwtToken)

	data, err := ParseJwtToken(secretKey, jwtToken)
	if err != nil {
		t.FailNow()
	}

	if data == nil {
		t.Fail()
	}

	t.Log("exp:", data["exp"])
	t.Log("iat:", data["iat"])
	t.Log(data)
	// t.Fail()
}
