package uuid

import (
	"fmt"
	"testing"
	"time"
)

func TestGenerateDeviceId(t *testing.T) {
	devIds := GenerateDeviceId(1)
	if devIds == nil {
		t.Fail()
	}

	if DEVICE_ID_STRING_LEN != len(devIds[0]) {
		t.Fail()
	}

	devIds = GenerateDeviceId(MaxGenerateDeviceCount)
	if devIds == nil {
		t.Fail()
	}

	devIds = GenerateDeviceId(MaxGenerateDeviceCount + 1)
	if devIds != nil {
		t.Fail()
	}

	devIds = GenerateDeviceId(0)
	if devIds != nil {
		t.Fail()
	}
}

func TestGenerateProductId(t *testing.T) {
	pid := GenerateProductId()
	if len(pid) != PRODUCT_ID_STRING_LEN {
		fmt.Println(pid, len(pid))
		t.Fail()
	}
}

func TestUidToString(t *testing.T) {
	tm := time.Now().Local().Unix()
	uid := UidToString(uint32(tm))
	if len(uid) != UID_STRING_LEN {
		fmt.Println(uid, len(uid))
		t.Fail()
	}
}
