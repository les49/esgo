package uuid

import "time"

var tableIdTimestamp uint32 = 0

func GenerateTableTimestampId() uint32 {
	tableId := uint32(time.Now().Local().Unix())

	if tableIdTimestamp < tableId {
		tableIdTimestamp = tableId
	} else {
		tableIdTimestamp = tableIdTimestamp + 1
	}

	return tableIdTimestamp
}
