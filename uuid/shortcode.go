package uuid

import (
	"encoding/binary"
	"time"

	"gitee.com/les49/esgo/encoding/base58"
)

var shortCodeIdTimestamp uint32 = 0

func GenerateShortCodeStringId() string {
	id := uint32(time.Now().Local().Unix())
	if shortCodeIdTimestamp < id {
		shortCodeIdTimestamp = id
	} else {
		shortCodeIdTimestamp = shortCodeIdTimestamp + 1
		id = shortCodeIdTimestamp
	}

	buf := make([]byte, binary.MaxVarintLen32)
	binary.BigEndian.PutUint32(buf, id)
	return base58.Encode(buf)
}
