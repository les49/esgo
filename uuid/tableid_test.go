package uuid

import "testing"

func TestGenerateTableTimestampId(t *testing.T) {
	lastId := GenerateTableTimestampId()
	t.Log(lastId)
	for i := 0; i < 10000; i++ {
		nowId := GenerateTableTimestampId()
		if lastId == nowId {
			t.FailNow()
		}
		lastId = nowId
	}
	t.Log(lastId)
}
