package uuid

import (
	"testing"
)

func TestGenerate(t *testing.T) {
	uuids := make([]uint64, MaxGenerateDeviceCount)
	sf := &SnowFlake{productId: 0}

	for i := 0; i < MaxGenerateDeviceCount; i++ {
		uuid, err := sf.Generate()
		if err != nil {
			t.Fail()
		}

		for j := 0; j < i; j++ {
			if uuid == uuids[j] {
				t.Fail()
			}
		}
		uuids = append(uuids, uuid)
	}
}
