package uuid

import (
	"encoding/binary"
	"time"

	"gitee.com/les49/esgo/encoding/base58"
)

func GenerateFileName() string {
	var filename string
	buf := make([]byte, binary.MaxVarintLen64)
	binary.BigEndian.PutUint64(buf, uint64(time.Now().UnixMilli()))
	filename = base58.Encode(buf)
	return toLen(filename, FILENAME_STRING_LEN)
}
