package uuid

import "testing"

func TestGenerateShortCodeStringId(t *testing.T) {
	idString := GenerateShortCodeStringId()
	if len(idString) == 0 {
		t.FailNow()
	}
	t.Log(idString)
}
